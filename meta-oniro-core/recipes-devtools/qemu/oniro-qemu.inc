# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: Apache-2.0

PACKAGECONFIG[libnfs] = "--enable-libnfs,--disable-libnfs,libnfs"
