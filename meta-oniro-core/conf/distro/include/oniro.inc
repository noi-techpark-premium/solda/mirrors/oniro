# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: Apache-2.0

INHERIT += "oniro-sanity"

# Default versions of toolchain components
# (can be overridden in kernel specific files if necessary)
BINUVERSION ?= "2.37%"
GCCVERSION ?= "11.%"
GLIBCVERSION ?= "2.33%"
